const gulp = require('gulp');
const plumber = require('gulp-plumber');
const watch = require('gulp-watch');
const notify = require('gulp-notify');

// SCSS
(() => {
	const sass = require('gulp-sass');
	const prefix = require('gulp-autoprefixer');

	gulp.task('css', () => {
		return gulp.src('./src/scss/main.scss')
		.pipe(plumber(function(e){
			console.log(e.message);
			this.emit('end');
		}))
		.pipe(sass({
			includePaths: ['./node_modules/'],
			indentType: 'tab',
			indentWidth: '1',
			outFile: './dist/css/main.css',
			outputStyle: 'compressed'
		}))
		.pipe(prefix("last 10 version", "> 1%", "ie 8", "ie 7"))
		.pipe(gulp.dest('./dist/css/'))
		.pipe(notify('CSS compilado!'))
		.on('error', () => {
			this.emit('end');
		});
	});
	gulp.task('csswatch', () => {
		watch('./src/scss/**/*.scss', () => {
			gulp.start('css');
		});
	});
})();

// JS
(() => {
	const babel = require('gulp-babel');
	const concat = require('gulp-concat');
	const sourcemaps = require('gulp-sourcemaps');

	gulp.task('js', () => {
		return gulp.src('./src/**/*.js')
		.pipe(plumber(function(e){
			console.log(e.message);
			this.emit('end');
		}))
		.pipe(sourcemaps.init())
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(concat('app.js'))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('dist/js'))
		.pipe(notify('JS compilado!'));
	});

	gulp.task('jswatch', () => {
		watch('./src/**/*.js', () => {
			gulp.start('js');
		});
	});
})();

gulp.task('default', ['css', 'js']);
gulp.task('watch', ['csswatch', 'jswatch']);