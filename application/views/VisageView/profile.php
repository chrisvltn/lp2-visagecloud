<!DOCTYPE html>
<html lang="pt">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Christian Valentin</title>

	<link rel="stylesheet" href="<?= base_url('dist/css/main.css') ?>" />
</head>

<body>
    <div class="container pt-3 pb-3">
        <div class="row">
            <div class="col-12">

                <!--Author box-->
                <div class="author-box">
                    <!--Name-->
                    <h3 class="h3-responsive text-center">Christian Valentin - 166088-8</h3>
                    <hr />
                    <div class="row">
                        <!--Avatar-->
                        <div class="col-12 col-sm-4">
                            <img src="<?= base_url('/dist/img/avatar.jpg') ?>" class="img-fluid rounded-circle z-depth-2">
                        </div>
                        <!--Author Data-->
                        <div class=" col-12 col-sm-8 mt-3">
                            <p><strong>Christian Valentin</strong></p>
                            <div class="personal-sm">
                                <a href="mailto:chris_valentin@outlook.com" class="email-ic"><i class="fa fa-envelope-o"> </i></a>
                                <a href="https://github.com/chrisvltn" class="github-ic"><i class="fa fa-github"> </i></a>
                                <a href="https://www.linkedin.com/in/christian-valentin/" class="li-ic"><i class="fa fa-linkedin"> </i></a>
                            </div>


                            <h4 class="mt-3"><strong>Visage Cloud</strong></h4>
                            <p>API para reconhecimento facial.</p>

                            <strong>Funcionalidades</strong>
                            <ul>
                                <li>Reconhecimento facial</li>
                                <li>Análise facial</li>
                            </ul>

                            <strong>Como funciona?</strong>
                            <p>A API separa as pessoas por coleções. Cada coleção possui uma variedade de perfis, e cada perfil possui seu rosto.</p>
                            <p>Ao fazer o reconhecimento facial, a API compara a foto com todos os perfis da coleção selecionada, e apresenta para o usuário a porcentagem de semelhança com todos os perfis que são, pelo menos, parecidos.</p>
                            <p>Ao fazer a análise da face, a API apresenta para o usuário alguns dados sobre a face, como a cor dos olhos, cor do cabelo, faixa de idade, sexo, etc. Na demonstração da API é feita a análise apenas de um rosto por foto, mas é possível analisar mais de um rosto de vez.</p>

                            <strong>Como acessar a demonstração?</strong>
                            <p>A demo pode ser acessada por <a href="<?= base_url('index.phpVisageView/index') ?>">esse site</a>.</p>

                            <strong>Sobre o código</strong>
                            <p>Foi utilizada uma classe VisageCloud como <strong>library</strong> para acessar e enviar requisições à API do VisageCloud, uma <strong>model</strong> para utilizar a classe da API e registrar os resultados no banco de dados, e um <strong>controller</strong> para executar as requisições AJAX ta página, e tornar a usabilidade mais otimizada.</p>
                            <p>Todas as classes e métodos foram documentados, e podem ser acessado <a target="_blank" href="<?= base_url('phpdoc') ?>">por esse link para o PHPDoc</a></p>
                        </div>
                    </div>
                </div>
                <!--/.Author box-->

            </div>
        </div>
    </div>
</body>

</html>
