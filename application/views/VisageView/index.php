<!DOCTYPE html>
<html lang="pt">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Visage Cloud Test</title>

	<link rel="stylesheet" href="<?= base_url('dist/css/main.css') ?>" />
</head>
<body>

	<div class="ch1-page pt-3 pb-5">
		<div class="container-fluid">

			<div class="row">
				<div class="col-12">
					<a class="float-right" href="<?= base_url('index.php/VisageView/profile') ?>"><strong>Acessar perfil</strong></a>

					<h1 class="h1-responsive text-center mb-5">
						Visage Cloud API
					</h1>
				</div>
			</div>

			<div class="row">

				<!-- Collection  -->
				<div class="col-12 col-sm-6 col-md-4">
					<div class="collection-ch1-wrapper">
						<div class="card">
							<div class="card-block">
								<label for="collection-ch1-list"><h4 class="card-title">Collection list</h4></label>
								<div class="card-text">
									<div class="form-group">
										<select name="collection-ch1-list" class="form-control collection-ch1-list">
											<option disabled selected>Choose one option</option>
										</select>
									</div>
								</div>
							</div>

							<div class="card-block">
								<form method="POST" action="https://legalbit.com.br/public/christian/index.php/VisageAPI/collection">
									<label for="collection-ch1-new"><h4 class="card-title">New Collection</h4></label>
									<div class="card-text">
										<div class="md-form">
											<input type="text" name="collectionName" id="collection-ch1-new" class="form-control" />
											<label for="collection-ch1-new">Type here</label>
										</div>
									</div>
									<button type="submit" class="btn btn-primary">Create</button>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- Collection  -->

				<!-- Profile -->
				<div class="col-12 col-sm-6 col-md-4">
					<div class="profile-ch1-wrapper">
						<div class="card">
							<div class="card-block">
								<label for="profile-ch1-list"><h4 class="card-title">Profile list</h4></label>
								<div class="card-text">
									<div class="form-group">
										<select name="profile-ch1-list" class="profile-ch1-list form-control">
											<option disabled selected>Choose one option</option>
										</select>
									</div>
								</div>
							</div>

							<div class="card-block">
								<form action="">
									<label for="profile-ch1-name"><h4 class="card-title">New Profile</h4></label>
									<div class="card-text">
										<div class="md-form">
											<input name="profileName" type="text" id="profile-ch1-name" class="form-control" />
											<label for="profile-ch1-name">Type the profile name here</label>
										</div>
										<div class="md-form">
											<input name="profileLabel" type="text" id="profile-ch1-tag" class="form-control" />
											<label for="profile-ch1-tag">Type the profile tags here</label>
										</div>
									</div>
									<button type="submit" class="btn btn-primary">Create</button>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- Profile -->

				<!-- Faces -->
				<div class="col-12 col-sm-6 col-md-4">
					<div class="face-ch1-wrapper">
						<div class="card">
							<div class="card-block">
								<label for="face-ch1-list"><h4 class="card-title">Face list</h4></label>
								<div class="card-text">

									<div class="form-group">
										<select name="face-ch1-list" class="face-ch1-list form-control">
											<option disabled selected>Choose one option</option>
										</select>
									</div>

									<div class="face-ch1-preview">
										<img src="" alt="" class="img-fluid" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Faces -->

				<!-- Face Analysis -->
				<div class="col-12 mt-3">
					<div class="face-analisys-ch1-wrapper">
						<div class="card">
							<div class="card-block">
								<label for="picture"><h4 class="card-title">Face analysis</h4></label>
								<div class="card-text">
									<form action="" method="POST">
										<div class="row">
											<div class="col-12 col-sm-6">
												<div class="form-group mt-3">
													<label for="picture" class="custom-file">
														<input type="file" name="picture" class="picture-ch1 custom-file-input">
														<span class="custom-file-control"></span>
													</label>
												</div>
											</div>
											<div class="col-12 col-sm-6">
												<div class="md-form">
													<input type="text" id="imageUrl-ch1" class="imageUrl-ch1 form-control" name="imageUrl" />
													<label for="imageUrl-ch1">Or type the image Url here...</label>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-12 col-sm-6">
												<div class="image-ch1-wrapper">
													<img src="" alt="" class="img-fluid" />
												</div>
											</div>

											<div class="col-12 col-md-6">
												<table class="table"></table>
											</div>

											<div class="col-12">
												<button class="btn btn-primary analyse-ch1-btn" hidden type="submit">Analyze</button>
												<button class="btn btn-primary send-ch1-btn" hidden type="submit">Apply to profile</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Face Analysis -->

				<!-- Face Recognition -->
				<div class="col-12 mt-3">
					<div class="face-recognition-ch1-wrapper">
						<div class="card">
							<div class="card-block">
								<form>
									<label for="picture"><h4 class="card-title">Face Recognition</h4></label>
									<div class="card-text">
										<div class="row">
											<div class="col-12 col-sm-6">
												<div class="form-group mt-3">
													<label for="picture" class="custom-file">
														<input type="file" name="picture" class="picture-ch1 custom-file-input">
														<span class="custom-file-control"></span>
													</label>
												</div>
											</div>
											<div class="col-12 col-sm-6">
												<div class="md-form">
													<input type="text" id="imageUrl-ch1-recognition" class="imageUrl-ch1-recognition form-control" name="imageUrl" />
													<label for="imageUrl-ch1-recognition">Or type the image Url here...</label>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-12">
												<div class="image-ch1-wrapper">
													<img src="" alt="" class="img-fluid" />
												</div>
											</div>

											<div class="col-12">
												<ul class="reference-ch1-list">
												</ul>
											</div>
										</div>

										<div class="row">
											<div class="col-12">
												<button type="submit" class="recognize-ch1-btn btn btn-primary" hidden>Recognize</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- Face Recognition -->

			</div>

		</div>
	</div>

	<script src="<?= base_url('dist/js/jquery.min.js') ?>"></script>
	<script src="<?= base_url('dist/js/tether.min.js') ?>"></script>
	<script src="<?= base_url('dist/js/bootstrap.min.js') ?>"></script>
	<script src="<?= base_url('dist/js/mdb.min.js') ?>"></script>
	<script src="<?= base_url('dist/js/app.js') ?>"></script>
</body>
</html>
