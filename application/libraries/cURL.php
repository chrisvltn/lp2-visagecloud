<?php

class cURL {
	/**
	 * Makes a POST Request
	 *
	 * @param string $url Request's URL
	 * @param array $params Request's parameters
	 * @param array $opt (optional) Additional options to the request
	 * @return void
	 */
	public static function post($url, $params, $opt = false) {
		$curl = curl_init();

		curl_setopt_array($curl, array(
		    CURLOPT_RETURNTRANSFER => 1,
		    CURLOPT_URL => $url,
		    CURLOPT_POST => 1,
			CURLOPT_SAFE_UPLOAD => false,
		    CURLOPT_POSTFIELDS => http_build_query($params),
		    CURLOPT_HTTPHEADER => array(
		    	'Content-Type' => 'application/x-www-form-urlencoded'
		    )
		));

		if(is_array($opt))
			curl_setopt_array($curl, $opt);

		return curl_exec($curl);
	}

	/**
	 * Creates a cURL file object
	 *
	 * @param string $file File path
	 * @param string $type File type (default: text/plain)
	 * @param string $name File name (default: curlFile)
	 * @return object cURL file object
	 */
	public static function file($file, $type = 'text/plain', $name = 'curlFile') {
		return curl_file_create($file, $type, $name);
	}
}
