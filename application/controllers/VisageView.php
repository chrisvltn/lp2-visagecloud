<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Components' view
 */
class VisageView extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->view('VisageView/index');
	}

	public function profile() {
		$this->load->view('VisageView/profile');
	}

}

/* End of file VisageView.php */
/* Location: ./application/controllers/VisageView.php */
