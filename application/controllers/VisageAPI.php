<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * API Class
 * @author Christian Valetnin <chris_valentin@outlook.com>
 */
class VisageAPI extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		header('Content-Type: application/json;charset=UTF-8', true);
		header('Access-Control-Allow-Credentials: true', true);

		$this->load->model('VisageCloudModel', 'vc');
	}

	/**
	 * Return a collection detail or creates a collection
	 *
	 * @param string $id Collection's id
	 * @return void
	 */
	public function collection($id = '') {
		if($this->input->method() == 'get') $this->getCollection($id);
		if($this->input->method() == 'post') $this->postCollection();
	}

	/**
	 * Get a collection based in its ID
	 * Prints a JSON with collection's name, and CollectionID
	 * @param Integer|String $id Collection's ID
	 * @return void
	 */
	private function getCollection($id){
		$this->db->select('name, VC_CollectionID');

		if(!empty($id))
			$this->db->where('id', $id);

		$res = $this->db->get('collection')->result();
		echo json_encode($res);
	}

	/**
	 * Post a collection with POST body 'collectionName'
	 * @return void
	 */
	private function postCollection() {
		$this->requireParams('collectionName', 'post');
		$resp = $this->vc->postCollection($this->input->post('collectionName'));
		echo json_encode($resp);
	}

	/**
	 * Return a profile detail or creates a profile
	 *
	 * @param string $id Profile's ID
	 * @return void
	 */
	public function profile($id = '') {
		if($this->input->method() == 'get') $this->getProfile($id);
		if($this->input->method() == 'post') $this->postProfile();
	}

	/**
	 * Get a profile based in its ID
	 * Prints JSON with profile's name, labels, CollectionID, and ProfileID
	 * @param Integer|String $id
	 * @return void
	 */
	private function getProfile($id){
		$this->db->select('name, labels, VC_CollectionID, VC_ProfileID');

		$this->filterRequest();
		if(!empty($id))
			$this->db->where('id', $id);

		$res = $this->db->get('profile')->result();
		echo json_encode($res);
	}

	/**
	 * Post a collection with POST body profileName, profileLabel, and collectionId
	 * @return void
	 */
	private function postProfile() {
		$this->requireParams('profileName,profileLabel,collectionId', 'post');

		$externalId = $this->input->post('externalId');
		if(empty($externalId))
			$externalId = false;

		$resp = $this->vc->postProfile($this->input->post('profileName'), $this->input->post('profileLabel'), $this->input->post('collectionId'), $externalId );
		echo json_encode($resp);
	}

	/**
	 * Return a face detail or creates a face
	 *
	 * @param string $id Face's id
	 * @return void
	 */
	public function face($id = '') {
		if($this->input->method() == 'get') $this->getFace($id);
		if($this->input->method() == 'post') $this->postFace();
	}

	/**
	 * Prints a JSON with the face imagePath, collectionName, profileName, labels, and profileId
	 *
	 * @param integer|string $id Face's id
	 * @return void
	 */
	private function getFace($id) {
		$this->db->select('f.imagePath, c.name as "collectionName", p.name as "profileName", p.labels, p.VC_ProfileID as profileId')
			->from('face as f')
			->join('collection as c', 'f.VC_CollectionID = c.VC_CollectionID', 'inner')
			->join('profile as p', 'f.VC_ProfileID = p.VC_ProfileID', 'inner');

		$this->filterRequest();
		if(!empty($id)) {
			$this->db->where('f.id', $id);
		}

		$res = $this->db->get()->result();
		echo json_encode($res);
	}

	/**
	 * Creates a face based on its faceHash, collectionId, profileId, and imageUrl
	 * @return void
	 */
	private function postFace() {
		$this->requireParams('faceHash,collectionId,profileId,imageUrl');
		$resp = $this->vc->faceToProfile($this->input->post('faceHash'), $this->input->post('collectionId'), $this->input->post('profileId'), $this->input->post('imageUrl') );
		echo json_encode($resp);
	}

	/**
	 * Reset all of its content to the default content
	 * @return void
	 */
	public function reset() {
		// DELETE FROM vc_collection WHERE creation_time >  '2017-06-25 00:00:00'
		$this->db
			->where("creation_time > '2017-06-25 00:00:00'")
			->delete('vc_collection');
		$this->db
			->where("creation_time > '2017-06-25 00:00:00'")
			->delete('vc_face');
		$this->db
			->where("creation_time > '2017-06-25 00:00:00'")
			->delete('vc_profile');
	}

	/**
	 * Requires a list of parameters, if one or more don't exist, it will exit the code and print an error JSON
	 *
	 * @param array|string $paramsList List of parameters to be required
	 * @param string $method Method to be required (Default POST)
	 * @return void
	 */
	private function requireParams($paramsList, $method = 'post') {
		if(is_string($paramsList)) $paramsList = explode(',', $paramsList);

		if($this->input->method() !== strtolower($method))
			$this->showError($this->input->method() . ' requests are not valid for this url', 405);

		$notDefined = array();
		foreach($paramsList as $p) { $p = trim($p);
			if(empty($this->input->post($p)))
				$notDefined[] = $p;
		}

		if(count($notDefined) > 0)
			$this->showError('Missing ' . implode($notDefined, ', ') . ' parameter(s)', 406);

		return true;
	}

	/**
	 * Exit the code and print an error JSON
	 *
	 * @param string $errorText Error message
	 * @param integer $error Error HTTP Code (default 406)
	 * @return void
	 */
	private function showError($errorText, $error = 406) {
		http_response_code($error);
		$error = array('Error' => $errorText);
		exit(json_encode($error));
	}

	/**
	 * Generates a WHERE clause with the _fields and _values in the GET params
	 * @return void
	 */
	private function filterRequest() {
		if(
			!empty($this->input->get('_fields')) &&
			count($this->input->get('_fields')) == count($this->input->get('_values'))
		) {
			foreach($this->input->get('_fields') as $index => $value){
				$i = explode(",", $this->input->get('_values')[$index]);

				foreach ($i as $key => $val) {
					if($key == 0)
						$this->db->where((string) $value, (string) $val);
					else
						$this->db->or_where((string) $value, (string) $val);
				}

			}
		}
	}

}

/* End of file VisageAPI.php */
/* Location: ./application/controllers/VisageAPI.php */
