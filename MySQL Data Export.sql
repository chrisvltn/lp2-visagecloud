-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 01, 2017 at 03:51 PM
-- Server version: 5.5.55-cll
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `legalbit_visage_cloud`
--

-- --------------------------------------------------------

--
-- Table structure for table `vc_collection`
--

CREATE TABLE `vc_collection` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `VC_CollectionID` varchar(70) NOT NULL,
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_collection`
--

INSERT INTO `vc_collection` (`id`, `name`, `VC_CollectionID`, `creation_time`) VALUES
(2, 'Actors', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', '0000-00-00 00:00:00'),
(3, 'Musician', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vc_face`
--

CREATE TABLE `vc_face` (
  `id` int(11) NOT NULL,
  `imagePath` varchar(2083) NOT NULL,
  `faceHash` varchar(70) NOT NULL,
  `VC_CollectionID` varchar(70) NOT NULL,
  `VC_ProfileID` varchar(70) NOT NULL,
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_face`
--

INSERT INTO `vc_face` (`id`, `imagePath`, `faceHash`, `VC_CollectionID`, `VC_ProfileID`, `creation_time`) VALUES
(28, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/4jki11i16kebd5840p3v0gf4omfgq2de8t5f0mel6ikqegaqreai92r7ljt6vavt', '9bf5e69718abd81fdadc832ee7b3921b99bc731906eb3a3b2a143f7f58412594', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', '1cgi62kf28g5ehi0qgtau0ono76p0c73e2hafiflp8u5ica38p4g22v3pouf1k6i', '0000-00-00 00:00:00'),
(27, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/seft6qna310bg9g87gs7vgsihoeq6gfthrn9cl0gfvfcoo472oabbes9hvqcsvsv', '07a6a69ac2d7564237fae6d1cd3b5fb22abe44b70d717a11c469686c135f7cbb', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'l8ccfjfrrlb8nsv5srrcgsfgmjidj6tghvgq3fhrcferhndaduq7j2fathehkvmn', '0000-00-00 00:00:00'),
(26, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/adlt3frug997iusod17d1lra1ot84dapsokftdeeef5den6vuh9pr55r2casb8l3', '01128ff5650d135ae6446c7cfccdaee618ec0ba51edcaa6eb0ce5987b6534eda', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'p02t483m632gbt2sls2urkgqlra1rsa4b37t8r0sc5hfeh2148u3si29v8abpga3', '0000-00-00 00:00:00'),
(25, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/hma723t739huovgt5dshdgq4rqbcij8ct2c5bsl2tglp0krlssu1emm3p750nq8n', 'c87c2c3fa53fc545bb0e242d9289b693d854c0f847a6f53b3c62db83303ded8d', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'mmbim9kap0m2p2ka8qnc6jq5879hji4pjpfdept1aciepns48cp0vt77afd87sci', '0000-00-00 00:00:00'),
(24, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/hr0fhnqthm5a0ui7qb44j68pqsduu4bui1kuuc3d0csdkf1cpo0aiuou9agc7p2l', '0d307544aa707a916fdc72f9bf479097a755531f6df182faf42c6f677c16d687', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'ag693iuie866e7hae7r1uaats156j781a6pm0svgo1ilka9404b1nqoup2ki6pj0', '0000-00-00 00:00:00'),
(23, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/4gj4gslacr41snab2eeeatmklq7j7sloajihqievbq84vs1unnumii9c3oii4v1c', '4d665e5a56ad1463d3c0a60b6ee9189b8cf6ba808698b423541308f32870752d', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'c3ov6vn3uev1jroq26k9bmlf55k4fg3d0bg3q95321n7k9s30ru127kgv3fntomq', '0000-00-00 00:00:00'),
(22, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/578gpqlef05hno5h7fmuubssmijrkgi10iocllcdumdtkh1sulk32eb1e95qigfd', '0aa938864ce28cc0efacd74d964fbae1395116155e74e8804e4207c53c3f5ddf', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'ai37tqpolq7q88t77o8ucab48ak68bffkdmjgg087ih9dpi1cve6p9r9tpm7c9q5', '0000-00-00 00:00:00'),
(21, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/pfs942qkddh2eegdhn43uh0pv4ma5qvgmmf6kgbs2ei0jscdgb970gjtoi25fluc', '3059c5fb09b8eacb84bf89b49ae33a225d9a66c996f3a7b90a075ce1863500d1', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'nc5riasg73bj76s51f9cugkco46j5rq91mebvsmqqldrgfn9um3ambh4dripi1dr', '0000-00-00 00:00:00'),
(16, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/h5o73og6a021np4v8spj110ks88cl0uvp34ppiilk3q9truhlc7lpc0srrk70d7v', '2db9c34e79a23eda4cf6ed9ea7d138ef5cc3781dc7155a915ca8e6f584a9b39d', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', '7ptn8g1h2a178etieqrhh9paa80f6dbltqgoh0f3fad7ph9d8oetedbdj7il3rdl', '0000-00-00 00:00:00'),
(15, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/iip57mi2jpke6baba3alpholei6e4ijvrtr4c8873gbnrrkoq3usv39abmm18jid', 'e2530bdba95d7a60c943b4fe4355a80021727f0969e037c907681f4f5f064c59', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', 'el574i18sjhipqk8cbmah2h61bb12jtaak141gp68h57fbel8mi22cc962t48728', '0000-00-00 00:00:00'),
(14, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/jl2jit32po0pt8ekrdfk4a0079o8ee3rhf6mpuvdj7g0blvibblmgkbfoqpcalqj', '1e8cd21b2cf82e37add208ce58c94abbdb55660efbed80a0b145fc885d87dbea', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', '32aue74p9tr2jf580fpracqaejugsnm4vl0t5ddoahmhqr90csjgvmpvfkab9vd', '0000-00-00 00:00:00'),
(13, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/rl7eqv6f0d8pi3gg2phfn7nf79525upgo43ui7d5unp7nvfs9hun846r4aq9pd6n', 'a9ef7b0ee8afc4eaf4e4703ba199529f2dc93ba8fa648a065092dc5421229a68', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', 'bsqdr9rha7pifijiptbhicumlfhokc9ra6pb2v4e18ufobsv5u2fn9utdjg88nc2', '0000-00-00 00:00:00'),
(12, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/a0o2pq4bpucsbgo9ehhbf5khp7rmcsk3cvtl2kmsvrjetr7l6h9ujd64snabbqc9', '3931c3550f34d4e483e67633e42e1a935fe5d3ce634fdfd0d910da7e70c4506a', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', 'lr49ef3ojtol93n3dk53fag1kjhp0jua1f8md88o78c3bd3d71vahf4ao25m8qs', '0000-00-00 00:00:00'),
(11, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/jfa3p0mdo0u6li1shiibkrc3h95febkhgj88n1djrp42sou79utvn51i6u1sdo4f', '6a2a635064f79b39de0e64f0b9751a0f3e95732c5fd69d61e59395b4b0b41ad4', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', '3o2potcgtjrb991tqr52go5nlc6ngg8l5tgd26t98dvn11dfdlntlibri38s4b52', '0000-00-00 00:00:00'),
(10, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/i8irmmj0uuqtfekij9kt5ma4nq79ml076odel2bemsaav7rj8dn9f2v0ojh92s55', '647ae3b76de9f2189630e98d8a50242b4cb98b7534413d23741e6d9a202538e3', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', 'c8jmt5q6ab89tp3e6antlfl57dnlq19b8i54ohmnnibeaknbf5ago7amarklhdh2', '0000-00-00 00:00:00'),
(9, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/hak46ksfr2a96kq5rngke2snao31jels78be6stce4nhq24jhglgkqbv8hups3u', 'fe39b342d8ed6e68f4f85da6e93122bbcca2e2a094559248f4c98f3008c5f601', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', 'gff46cv09ompst6d6c1vdkat25hpqega3cmfalopciou2dt6tgs6q09jru6n2r94', '0000-00-00 00:00:00'),
(8, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/n0rn9n0m536168rj0cs92sam5jfvhbtm8bu9iv7d0au07hhtuk47ov0plcchouln', '5a8706a69d0e3a75f31a98fd38adb984bb1fa6cd797f794f80ef2be80fc1d32f', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', '1onplhbgtcdo69o9s1ap5k5t90o9ojl0bg0pqjdi5t56vqlj31q0imqm5ho27616', '0000-00-00 00:00:00'),
(7, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/p8ll5oisf6elkjakk76d5jap16uipkqhltbmmeg0f5u533b17ksj9fv3m0oe6oc', 'a6574c5626c2a16f782472e6b3edb7ff7e358c359c5d64374e7fbd2dfbae680d', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', '20affb3hu5irsdnic5mimm27l5s0flqc7cph8kdmiqu69l7gkiift43nkg3caufk', '0000-00-00 00:00:00'),
(6, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/65eee0uhuvop36vd42ckm5g99avan13jao8j187060akbav6igvspgmt877qt620', '95a8fe0a74ae5304cbe53e13f7a4748a55b53640bf529a61e1d2d3475d4f9425', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', 'lrnt8tpj37qc2v3ldaj80bl537g6aiuj2b0uof5m36kej7b6mf426fi6af6mffsp', '0000-00-00 00:00:00'),
(5, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/e3o0hovme77at8s2tadl0l00avnolgikg2efmm833iagjbpjrqp82pl2ibodmib4', 'b6a11d667db3c54f9976425bb486e2fe05e0fe66c0e6e56e5b1157498e367692', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', '4574vfb2le2nl2tcpq9ab3vd2bmklcrpmnr2rqs3ndb85htcl0tn3i47nvp071af', '0000-00-00 00:00:00'),
(17, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/d7qepch4cuemjq32grtmpbmk6742u3sut268mg60aj6pu0k1fdb33s81q6flbl9p', '95142c975bdf807f495768650543231a7ecd83a10031b98513bdd811d55acd91', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', '4bp2ab063f2fhs8c02k6hpki8s3tetq8880kntb840rpo37gu7bfsoqcipf3icd8', '0000-00-00 00:00:00'),
(18, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/nuotqincv5nr49d8u9fovqd1uj1tbo9idhmc7fndm95tt57opr5ic3v5cab3fq4k', 'fd2e53280a218d6f76eec3524ddd7d54fec0b63f93e509a1f663d4bbd35b5887', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'tmdbeg478aua0g7ejn69fl6n0ii8m3adrr1q67lqadehlcfci7vluc27odbip1u8', '0000-00-00 00:00:00'),
(19, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/4tgev3ruluoefup2adv5nb03ocq72mv5497ffb6an4pm31djkcv88014r37ohbnp', 'd07422beb5fec47eb982071c4350caf3df27ebb901f1a5fae321f94e04a297b0', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'lks63krd22lngrum2pn2dd4kpumuche2lpmkt3m6vefrgk6maqd66lg6rjgn0201', '0000-00-00 00:00:00'),
(20, 'https://s3.eu-central-1.amazonaws.com/visagecloud-ops-xd31vmo8gohlm8zjaexj/b20v7g6aa9tvn0225uutrv4v0j69f1k53ap8rfoehakbhhoi/4bl1tdt9aj80mrao19dut2ev7h2gp1kob8jdis9q2p26i1bvplghlogct6mbu41o', '184812027c2153bef78048c0f0e65a58e93e0c2635ab9455a09e7cff85e569ef', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', '3cgs3vd6u1uoliuol28tnmpju4bf3jocsjv455sadr492pvp0rcvvoi1s9t62fj2', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vc_profile`
--

CREATE TABLE `vc_profile` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `labels` varchar(50) DEFAULT NULL,
  `VC_CollectionID` varchar(70) NOT NULL,
  `VC_ProfileID` varchar(70) NOT NULL,
  `creation_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_profile`
--

INSERT INTO `vc_profile` (`id`, `name`, `labels`, `VC_CollectionID`, `VC_ProfileID`, `creation_time`) VALUES
(30, 'Axl Rose', 'singer,rock', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'ag693iuie866e7hae7r1uaats156j781a6pm0svgo1ilka9404b1nqoup2ki6pj0', '0000-00-00 00:00:00'),
(29, 'Bruno Mars', 'singer,pop', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'c3ov6vn3uev1jroq26k9bmlf55k4fg3d0bg3q95321n7k9s30ru127kgv3fntomq', '0000-00-00 00:00:00'),
(34, 'Lenny Kravitz', 'singer,rock', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', '1cgi62kf28g5ehi0qgtau0ono76p0c73e2hafiflp8u5ica38p4g22v3pouf1k6i', '0000-00-00 00:00:00'),
(26, 'Beyoncé', 'singer,pop', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'ai37tqpolq7q88t77o8ucab48ak68bffkdmjgg087ih9dpi1cve6p9r9tpm7c9q5', '0000-00-00 00:00:00'),
(25, 'Adele', 'singer,pop', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'nc5riasg73bj76s51f9cugkco46j5rq91mebvsmqqldrgfn9um3ambh4dripi1dr', '0000-00-00 00:00:00'),
(20, 'Benedict Cumberbatch', 'actor', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', '7ptn8g1h2a178etieqrhh9paa80f6dbltqgoh0f3fad7ph9d8oetedbdj7il3rdl', '0000-00-00 00:00:00'),
(19, 'Chris Patt', 'actor', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', 'el574i18sjhipqk8cbmah2h61bb12jtaak141gp68h57fbel8mi22cc962t48728', '0000-00-00 00:00:00'),
(18, 'Anne Hathaway', 'actress', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', '32aue74p9tr2jf580fpracqaejugsnm4vl0t5ddoahmhqr90csjgvmpvfkab9vd', '0000-00-00 00:00:00'),
(17, 'Emma Watson', 'actress', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', 'c8jmt5q6ab89tp3e6antlfl57dnlq19b8i54ohmnnibeaknbf5ago7amarklhdh2', '0000-00-00 00:00:00'),
(16, 'Krysten Ritter', 'actress', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', 'bsqdr9rha7pifijiptbhicumlfhokc9ra6pb2v4e18ufobsv5u2fn9utdjg88nc2', '0000-00-00 00:00:00'),
(15, 'Viola Davis', 'actress', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', 'lr49ef3ojtol93n3dk53fag1kjhp0jua1f8md88o78c3bd3d71vahf4ao25m8qs', '0000-00-00 00:00:00'),
(14, 'Emma Stone', 'actress', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', '3o2potcgtjrb991tqr52go5nlc6ngg8l5tgd26t98dvn11dfdlntlibri38s4b52', '0000-00-00 00:00:00'),
(13, 'Jim Carrey', 'actor,comedy', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', 'gff46cv09ompst6d6c1vdkat25hpqega3cmfalopciou2dt6tgs6q09jru6n2r94', '0000-00-00 00:00:00'),
(12, 'Pai do Chris', 'actor', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', 'lrnt8tpj37qc2v3ldaj80bl537g6aiuj2b0uof5m36kej7b6mf426fi6af6mffsp', '0000-00-00 00:00:00'),
(9, 'John Cena', 'actor,fighter', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', '4574vfb2le2nl2tcpq9ab3vd2bmklcrpmnr2rqs3ndb85htcl0tn3i47nvp071af', '0000-00-00 00:00:00'),
(10, 'Evan Peters', 'actor', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', '1onplhbgtcdo69o9s1ap5k5t90o9ojl0bg0pqjdi5t56vqlj31q0imqm5ho27616', '0000-00-00 00:00:00'),
(11, 'Johnny Depp', 'actor,crazy', 'inhh20104p9luabqal998nhcaqiqsvita8ijd39rho2dm0lo8p7itqg9nro7t19m', '20affb3hu5irsdnic5mimm27l5s0flqc7cph8kdmiqu69l7gkiift43nkg3caufk', '0000-00-00 00:00:00'),
(21, 'Will I Am', 'singer,black eyed peas', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', '3cgs3vd6u1uoliuol28tnmpju4bf3jocsjv455sadr492pvp0rcvvoi1s9t62fj2', '0000-00-00 00:00:00'),
(22, 'Fergie', 'singer,black eyed peas', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'lks63krd22lngrum2pn2dd4kpumuche2lpmkt3m6vefrgk6maqd66lg6rjgn0201', '0000-00-00 00:00:00'),
(23, 'Kurt Cobain', 'singer', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'tmdbeg478aua0g7ejn69fl6n0ii8m3adrr1q67lqadehlcfci7vluc27odbip1u8', '0000-00-00 00:00:00'),
(24, 'Dave Grohl', 'singer,rock', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', '4bp2ab063f2fhs8c02k6hpki8s3tetq8880kntb840rpo37gu7bfsoqcipf3icd8', '0000-00-00 00:00:00'),
(31, 'Ronnie Radke', 'singer,emo', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'mmbim9kap0m2p2ka8qnc6jq5879hji4pjpfdept1aciepns48cp0vt77afd87sci', '0000-00-00 00:00:00'),
(32, 'Lana Del Rey', 'singer,sad', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'p02t483m632gbt2sls2urkgqlra1rsa4b37t8r0sc5hfeh2148u3si29v8abpga3', '0000-00-00 00:00:00'),
(33, 'Joan Jett', 'singer,rock', 'nqvglog0gqc1dp3uk8hcnuqocilou6kv4a2ccq3s5h0usj7na9h8l54n3t0es6na', 'l8ccfjfrrlb8nsv5srrcgsfgmjidj6tghvgq3fhrcferhndaduq7j2fathehkvmn', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vc_collection`
--
ALTER TABLE `vc_collection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_face`
--
ALTER TABLE `vc_face`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vc_face1` (`VC_CollectionID`),
  ADD KEY `fk_vc_face2` (`VC_ProfileID`);

--
-- Indexes for table `vc_profile`
--
ALTER TABLE `vc_profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vc_profile` (`VC_CollectionID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vc_collection`
--
ALTER TABLE `vc_collection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `vc_face`
--
ALTER TABLE `vc_face`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `vc_profile`
--
ALTER TABLE `vc_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
