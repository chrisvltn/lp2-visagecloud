class Collection {
	constructor(name) {
		this.name = name.trim();
	}
	static getAll() {
		return $.ajax({
			url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/collection/',
			type: 'GET'
		})
	}
	static get(id) {
		return $.ajax({
			url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/collection/' + id,
			type: 'GET'
		})
	}
	static getCollectionId() {
		return $('.collection-ch1-wrapper').find('.collection-ch1-list').val()
	}
	insert() {
		return $.ajax({
			url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/collection',
			type: 'POST',
			data: {
				'collectionName': this.name
			}
		})
	}
}

$(() => {
	var wrapper = $('.collection-ch1-wrapper')
	var list = wrapper.find('.collection-ch1-list')
	var card = wrapper.find('.card')

	const cardLoad = a => a ? card.addClass('loading') : card.removeClass('loading')
	const loadCollections = () => {
		cardLoad(1)
		return Collection.getAll().done(d => {
			if(!d.length) return cardLoad(0)

			var html = ''
			for(var item of d) {
				html += `<option value="${item.VC_CollectionID}">${item.name}</option>`
			}
			list.html(html).find('option').not('[disabled]').first().attr('selected', 'selected')
			list.change()

			cardLoad(0)
		});
	}

	wrapper.find('select').change(() => $('.profile-ch1-wrapper').add('.face-ch1-wrapper').trigger('reload'))

	wrapper.find('form').submit(function(e){
		cardLoad(1)
		e.preventDefault()

		var input = $(this).find('input[name="collectionName"]').removeClass('error')

		if(!input.val().length)
			return input.addClass('error')

		var col = new Collection(input.val())
		col.insert()
			.done(d => d.status.toLowerCase() != "ok" ? input.addClass('error') : null)
			.always(() => cardLoad(0))
			.done(() => {
				loadCollections()
					.done(() => list
								.find('option')
								.filter((i, el) => $(el).text() == col.name)
								.first()
								.attr('selected', 'true')
								.parent()
								.change() )
			})
			.done(() => input.val('').change())
	})

	loadCollections()
})