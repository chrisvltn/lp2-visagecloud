class Face {
	constructor(data) {
		if(typeof data == "string")
			this.imageUrl = data
		else
			this.file = data
	}
	static getAll() {
		if(!Collection.getCollectionId()) return;
		return $.ajax({
			url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/face',
			type: 'GET',
			data: {
				'_fields[]': 'c.VC_CollectionID',
				'_values[]': Collection.getCollectionId()
			}
		})
	}
	static get(profile) {
		return $.ajax({
			url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/face/',
			type: 'GET',
			data: {
				'_fields[]': 'p.VC_ProfileID',
				'_values[]': profile
			}
		})
	}
	static getFaceId() {
		return $('.face-ch1-wrapper').find('.face-ch1-list').val()
	}
	toProfile() {
		if(!Collection.getCollectionId()) return;
		if(!Profile.getProfileId()) return;
		return $.ajax({
			url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/face',
			type: 'POST',
			data: {
				'faceHash': this.faceHash,
				'collectionId': Collection.getCollectionId(),
				'profileId': Profile.getProfileId(),
				'imageUrl': this.imageUrl
			},
			success: () => $('.face-ch1-wrapper').trigger('reload')
		})
	}
	detect() {
		var data = new FormData()
		data.append('accessKey', 'n3q71bnt0gjv24ic0kvjssrke3mv5maplb0b543e49kfas2')
		data.append('secretKey', '4eo3voanfguiktmuf8edahbqt67vvflp97ib1a3cv2q3d1tth7nus4jeognajo8llou71rc1756ri5a29417lkrqr2dteqrb')
		data.append('storePicture', true)

		if(this.imageUrl)
			data.append('pictureURL', this.imageUrl)
		else
			data.append("picture", this.file, this.file.name);

		return $.ajax({
			url: 'https://visagecloud.com/rest/v1.1/analysis/detection',
			type: 'POST',
			processData: false,
			contentType: false,
			data: data,
			success: d => {
				this.imageUrl = d.payload.detection.storeAssetURL
				this.faceHash = d.payload.detection.faces["0"].hash
			}
		})
	}
	recognize() {
		if(!Collection.getCollectionId()) return;
		var data = new FormData()
		data.append('accessKey', 'n3q71bnt0gjv24ic0kvjssrke3mv5maplb0b543e49kfas2')
		data.append('secretKey', '4eo3voanfguiktmuf8edahbqt67vvflp97ib1a3cv2q3d1tth7nus4jeognajo8llou71rc1756ri5a29417lkrqr2dteqrb')
		data.append('collectionId', Collection.getCollectionId())

		if(this.imageUrl)
			data.append('pictureURL', this.imageUrl)
		else
			data.append("picture", this.file, this.file.name);

		return $.ajax({
			url: 'https://visagecloud.com/rest/v1.1/analysis/recognition',
			type: 'POST',
			processData: false,
			contentType: false,
			data: data
		})
	}
}

// FaceList
$(() => {
	var wrapper = $('.face-ch1-wrapper')
	var list = wrapper.find('.face-ch1-list').on('change', function() {
		var opt = $(this).children().filter((i, el) => $(el).attr('value') == $(this).val()).trigger('selected')
	})
	var card = wrapper.find('.card')
	var preview = wrapper.find('.face-ch1-preview img')

	const cardLoad = a => a ? card.addClass('loading') : card.removeClass('loading')
	const loadFaces = () => {
		cardLoad(1)
		return Face.getAll().done(d => {
			var items = new $
			preview.attr('src', '')

			var counter = 1
			for(let item of d) {
				items = items.add($(`<option value="${counter++}" >${item.profileName}</option>`).on('selected', () => {
					preview.attr('src', item.imagePath)
				}))
			}
			list.empty().append(items).find('option').not('[disabled]').first().attr('selected', 'selected')
			list.change()
			cardLoad(0)
		});
	}

	wrapper.on('reload', loadFaces)

	loadFaces()
})

const fileInputEvents = wrapper => {
	wrapper.find('input[name="picture"]').change(function() {
		var img = wrapper.find('.image-ch1-wrapper img')
		wrapper.find('input[name="imageUrl"]').val('')
		try {
			var reader = new FileReader()
			reader.onload = () => { img.attr('src', reader.result) }
			reader.readAsDataURL($(this)[0].files[0])
		} catch(e) {
			img.attr('src', '')
		}
	})

	wrapper.find('input[name="imageUrl"]').change(function() { wrapper.find('.image-ch1-wrapper img').attr('src', $(this).val()) })
}
// Face Analysis
$(() => {
	const wrapper = $('.face-analisys-ch1-wrapper')
	const preview = wrapper.find('.image-ch1-wrapper')
	var card = wrapper.find('.card')
	const cardLoad = a => a ? card.addClass('loading') : card.removeClass('loading')

	let face

	wrapper.find('form').submit(function(e) {
		e.preventDefault()
		cardLoad(1)

		var $t = $(this)
		var file = $t.find('input[name="picture"]')
		var imageUrl = $t.find('input[name="imageUrl"]')

		const showSendBtn = () => { wrapper.find('.send-ch1-btn').removeAttr('hidden') }
		const printTable = d => {
			var data = d.payload.detection.faces[0].attributes

			var html = `
				<tr>
					<th>Name</th>
					<th>Value</th>
					<th>👍</th>
				</tr>
			`

			for(var index in data) {
				data[index].value = data[index].value.toString().indexOf('#') == 0 ? `<span class="square" style="background-color: ${data[index].value}">${data[index].value}</span>` : data[index].value

				html += `
					<tr>
						<td> ${data[index].name} </td>
						<td> ${data[index].value} </td>
						<td> ${data[index].confidence.toFixed(2) * 100}% </td>
					</tr>
				`
			}

			wrapper.find('table.table').html(html)
			cardLoad(0)
		}
		const drawSquares = d => {
			var image = preview.find('img')[0]
			const ratio = image.naturalWidth / image.width

			preview.find('.dot').remove()

			var keypoints = d.payload.detection.faces["0"].keypoints

			for(var index in keypoints)
				for(var item of keypoints[index])
					preview.append(`<div style="transform: translateX(${item.x / ratio}px) translateY(${item.y / ratio}px);" class="dot" />`)
		}

		if(imageUrl.val().length){
			face = new Face(imageUrl.val())
			return face.detect()
				.done(console.log)
				.done(printTable)
				.done(showSendBtn)
				.done(drawSquares)
		}

		if(!file[0].files.length)
			return file.addClass('error')

		face = new Face(file[0].files[0])
		face.detect()
			.done(console.log)
			.done(printTable)
			.done(showSendBtn)
			.done(drawSquares)
	})

	wrapper.find('input').change(function() {
		wrapper.find('.analyse-ch1-btn').removeAttr('hidden')
	})

	wrapper.find('.send-ch1-btn').click(function(e) {
		e.preventDefault()
		cardLoad(1)
		face.toProfile()
			.always(() => cardLoad(0))
	})

	fileInputEvents(wrapper)
})

// Face Recognition
$(() => {
	const wrapper = $('.face-recognition-ch1-wrapper')
	const preview = wrapper.find('.image-ch1-wrapper')
	var card = wrapper.find('.card')
	const cardLoad = a => a ? card.addClass('loading') : card.removeClass('loading')

	let face

	wrapper.find('form').submit(function(e) {
		e.preventDefault()
		cardLoad(1)

		var $t = $(this)
		var file = $t.find('input[name="picture"]')
		var imageUrl = $t.find('input[name="imageUrl"]')

		const getFaces = profileList => Face.get(profileList.map(item => item.trim()).join(','))
		const insertComparison = d => {
			var comparison = d.payload.recognition.comparison
			var arr = []

			for(var index in comparison)
				for(var item of comparison[index])
					arr.push(item)

			getFaces(arr.map(item => item.id))
				.done(d => {
					var html = ''
					for(var item of d) {
						html += `<li><img src="${item.imagePath}" alt="${item.profileName}" /> <span>${item.profileName}</span> <span>${(arr.filter(i => i.id == item.profileId).pop().matchRate * 100).toFixed(2)}%</span> </li>`
					}
					wrapper.find('.reference-ch1-list').html(html)
					cardLoad(0)
				})
		}
		const drawSquares = d => {
			var image = preview.find('img')[0]
			const ratio = image.naturalWidth / image.width

			preview.find('.dot').remove()

			var keypoints = d.payload.detection.faces["0"].keypoints

			for(var index in keypoints)
				for(var item of keypoints[index])
					preview.append(`<div style="transform: translateX(${item.x / ratio}px) translateY(${item.y / ratio}px);" class="dot" />`)
		}

		if(imageUrl.val().length){
			face = new Face(imageUrl.val())
			return face.recognize()
				.done(console.log)
				.done(insertComparison)
				.done(drawSquares)
		}

		if(!file[0].files.length)
			return file.addClass('error')

		face = new Face(file[0].files[0])
		face.recognize()
			.done(console.log)
			.done(insertComparison)
			.done(drawSquares)
	})

	wrapper.find('input').change(function() {
		wrapper.find('.recognize-ch1-btn').removeAttr('hidden')
	})

	fileInputEvents(wrapper)
})