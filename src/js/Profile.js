class Profile {
	constructor(name, label) {
		this.name = name.trim();
		this.label = label.map(item => item.trim())
	}
	static getAll() {
		if(!Collection.getCollectionId()) return;
		return $.ajax({
			url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/profile/',
			type: 'GET',
			data: {
				'_fields[]': 'VC_CollectionID',
				'_values[]': Collection.getCollectionId()
			}
		})
	}
	static get(id) {
		return $.ajax({
			url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/profile/' + id,
			type: 'GET'
		})
	}
	static getProfileId() {
		return $('.profile-ch1-wrapper').find('.profile-ch1-list').val()
	}
	insert() {
		if(!Collection.getCollectionId()) return;
		return $.ajax({
			url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/profile',
			type: 'POST',
			data: {
				'collectionId': Collection.getCollectionId(),
				'profileName': this.name,
				'profileLabel': this.label.join(',')
			}
		})
	}
}

$(() => {
	var wrapper = $('.profile-ch1-wrapper')
	var list = wrapper.find('.profile-ch1-list')
	var card = wrapper.find('.card')

	const cardLoad = a => a ? card.addClass('loading') : card.removeClass('loading')
	const loadProfiles = () => {
		cardLoad(1)
		return Profile.getAll().done(d => {
			var html = ''
			for(var item of d) {
				html += `<option value="${item.VC_ProfileID}">${item.name}</option>`
			}
			list.html(html).find('option').not('[disabled]').first().attr('selected', 'selected')
			cardLoad(0)
		});
	}

	wrapper.on('reload', loadProfiles)

	wrapper.find('form').submit(function(e){
		cardLoad(1)
		e.preventDefault()

		var name = $(this).find('input[name="profileName"]').removeClass('error')
		var label = $(this).find('input[name="profileLabel"]').removeClass('error')

		if(!(name.val() || "").length)
			return name.addClass('error')
		if(!(label.val() || "").length)
			return label.addClass('error')

		var pro = new Profile(name.val(), label.val().split(','))
		pro.insert()
			.done(d => d.status.toLowerCase() != "ok" ? input.addClass('error') : null)
			.always(() => cardLoad(0))
			.done(() => {
				loadProfiles()
					.done(() => list
								.find('option')
								.filter((i, el) => $(el).text() == pro.name)
								.first()
								.attr('selected', 'true')
								.parent()
								.change() )
			})
			.done(() => $(this).find('input').val('').change())
	})

	loadProfiles()
})