'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Collection = function () {
	function Collection(name) {
		_classCallCheck(this, Collection);

		this.name = name.trim();
	}

	_createClass(Collection, [{
		key: 'insert',
		value: function insert() {
			return $.ajax({
				url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/collection',
				type: 'POST',
				data: {
					'collectionName': this.name
				}
			});
		}
	}], [{
		key: 'getAll',
		value: function getAll() {
			return $.ajax({
				url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/collection/',
				type: 'GET'
			});
		}
	}, {
		key: 'get',
		value: function get(id) {
			return $.ajax({
				url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/collection/' + id,
				type: 'GET'
			});
		}
	}, {
		key: 'getCollectionId',
		value: function getCollectionId() {
			return $('.collection-ch1-wrapper').find('.collection-ch1-list').val();
		}
	}]);

	return Collection;
}();

$(function () {
	var wrapper = $('.collection-ch1-wrapper');
	var list = wrapper.find('.collection-ch1-list');
	var card = wrapper.find('.card');

	var cardLoad = function cardLoad(a) {
		return a ? card.addClass('loading') : card.removeClass('loading');
	};
	var loadCollections = function loadCollections() {
		cardLoad(1);
		return Collection.getAll().done(function (d) {
			if (!d.length) return cardLoad(0);

			var html = '';
			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = d[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var item = _step.value;

					html += '<option value="' + item.VC_CollectionID + '">' + item.name + '</option>';
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}

			list.html(html).find('option').not('[disabled]').first().attr('selected', 'selected');
			list.change();

			cardLoad(0);
		});
	};

	wrapper.find('select').change(function () {
		return $('.profile-ch1-wrapper').add('.face-ch1-wrapper').trigger('reload');
	});

	wrapper.find('form').submit(function (e) {
		cardLoad(1);
		e.preventDefault();

		var input = $(this).find('input[name="collectionName"]').removeClass('error');

		if (!input.val().length) return input.addClass('error');

		var col = new Collection(input.val());
		col.insert().done(function (d) {
			return d.status.toLowerCase() != "ok" ? input.addClass('error') : null;
		}).always(function () {
			return cardLoad(0);
		}).done(function () {
			loadCollections().done(function () {
				return list.find('option').filter(function (i, el) {
					return $(el).text() == col.name;
				}).first().attr('selected', 'true').parent().change();
			});
		}).done(function () {
			return input.val('').change();
		});
	});

	loadCollections();
});
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Face = function () {
	function Face(data) {
		_classCallCheck(this, Face);

		if (typeof data == "string") this.imageUrl = data;else this.file = data;
	}

	_createClass(Face, [{
		key: 'toProfile',
		value: function toProfile() {
			if (!Collection.getCollectionId()) return;
			if (!Profile.getProfileId()) return;
			return $.ajax({
				url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/face',
				type: 'POST',
				data: {
					'faceHash': this.faceHash,
					'collectionId': Collection.getCollectionId(),
					'profileId': Profile.getProfileId(),
					'imageUrl': this.imageUrl
				},
				success: function success() {
					return $('.face-ch1-wrapper').trigger('reload');
				}
			});
		}
	}, {
		key: 'detect',
		value: function detect() {
			var _this = this;

			var data = new FormData();
			data.append('accessKey', 'n3q71bnt0gjv24ic0kvjssrke3mv5maplb0b543e49kfas2');
			data.append('secretKey', '4eo3voanfguiktmuf8edahbqt67vvflp97ib1a3cv2q3d1tth7nus4jeognajo8llou71rc1756ri5a29417lkrqr2dteqrb');
			data.append('storePicture', true);

			if (this.imageUrl) data.append('pictureURL', this.imageUrl);else data.append("picture", this.file, this.file.name);

			return $.ajax({
				url: 'https://visagecloud.com/rest/v1.1/analysis/detection',
				type: 'POST',
				processData: false,
				contentType: false,
				data: data,
				success: function success(d) {
					_this.imageUrl = d.payload.detection.storeAssetURL;
					_this.faceHash = d.payload.detection.faces["0"].hash;
				}
			});
		}
	}, {
		key: 'recognize',
		value: function recognize() {
			if (!Collection.getCollectionId()) return;
			var data = new FormData();
			data.append('accessKey', 'n3q71bnt0gjv24ic0kvjssrke3mv5maplb0b543e49kfas2');
			data.append('secretKey', '4eo3voanfguiktmuf8edahbqt67vvflp97ib1a3cv2q3d1tth7nus4jeognajo8llou71rc1756ri5a29417lkrqr2dteqrb');
			data.append('collectionId', Collection.getCollectionId());

			if (this.imageUrl) data.append('pictureURL', this.imageUrl);else data.append("picture", this.file, this.file.name);

			return $.ajax({
				url: 'https://visagecloud.com/rest/v1.1/analysis/recognition',
				type: 'POST',
				processData: false,
				contentType: false,
				data: data
			});
		}
	}], [{
		key: 'getAll',
		value: function getAll() {
			if (!Collection.getCollectionId()) return;
			return $.ajax({
				url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/face',
				type: 'GET',
				data: {
					'_fields[]': 'c.VC_CollectionID',
					'_values[]': Collection.getCollectionId()
				}
			});
		}
	}, {
		key: 'get',
		value: function get(profile) {
			return $.ajax({
				url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/face/',
				type: 'GET',
				data: {
					'_fields[]': 'p.VC_ProfileID',
					'_values[]': profile
				}
			});
		}
	}, {
		key: 'getFaceId',
		value: function getFaceId() {
			return $('.face-ch1-wrapper').find('.face-ch1-list').val();
		}
	}]);

	return Face;
}();

// FaceList


$(function () {
	var wrapper = $('.face-ch1-wrapper');
	var list = wrapper.find('.face-ch1-list').on('change', function () {
		var _this2 = this;

		var opt = $(this).children().filter(function (i, el) {
			return $(el).attr('value') == $(_this2).val();
		}).trigger('selected');
	});
	var card = wrapper.find('.card');
	var preview = wrapper.find('.face-ch1-preview img');

	var cardLoad = function cardLoad(a) {
		return a ? card.addClass('loading') : card.removeClass('loading');
	};
	var loadFaces = function loadFaces() {
		cardLoad(1);
		return Face.getAll().done(function (d) {
			var items = new $();
			preview.attr('src', '');

			var counter = 1;
			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				var _loop = function _loop() {
					var item = _step.value;

					items = items.add($('<option value="' + counter++ + '" >' + item.profileName + '</option>').on('selected', function () {
						preview.attr('src', item.imagePath);
					}));
				};

				for (var _iterator = d[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					_loop();
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}

			list.empty().append(items).find('option').not('[disabled]').first().attr('selected', 'selected');
			list.change();
			cardLoad(0);
		});
	};

	wrapper.on('reload', loadFaces);

	loadFaces();
});

var fileInputEvents = function fileInputEvents(wrapper) {
	wrapper.find('input[name="picture"]').change(function () {
		var img = wrapper.find('.image-ch1-wrapper img');
		wrapper.find('input[name="imageUrl"]').val('');
		try {
			var reader = new FileReader();
			reader.onload = function () {
				img.attr('src', reader.result);
			};
			reader.readAsDataURL($(this)[0].files[0]);
		} catch (e) {
			img.attr('src', '');
		}
	});

	wrapper.find('input[name="imageUrl"]').change(function () {
		wrapper.find('.image-ch1-wrapper img').attr('src', $(this).val());
	});
};
// Face Analysis
$(function () {
	var wrapper = $('.face-analisys-ch1-wrapper');
	var preview = wrapper.find('.image-ch1-wrapper');
	var card = wrapper.find('.card');
	var cardLoad = function cardLoad(a) {
		return a ? card.addClass('loading') : card.removeClass('loading');
	};

	var face = void 0;

	wrapper.find('form').submit(function (e) {
		e.preventDefault();
		cardLoad(1);

		var $t = $(this);
		var file = $t.find('input[name="picture"]');
		var imageUrl = $t.find('input[name="imageUrl"]');

		var showSendBtn = function showSendBtn() {
			wrapper.find('.send-ch1-btn').removeAttr('hidden');
		};
		var printTable = function printTable(d) {
			var data = d.payload.detection.faces[0].attributes;

			var html = '\n\t\t\t\t<tr>\n\t\t\t\t\t<th>Name</th>\n\t\t\t\t\t<th>Value</th>\n\t\t\t\t\t<th>\uD83D\uDC4D</th>\n\t\t\t\t</tr>\n\t\t\t';

			for (var index in data) {
				data[index].value = data[index].value.toString().indexOf('#') == 0 ? '<span class="square" style="background-color: ' + data[index].value + '">' + data[index].value + '</span>' : data[index].value;

				html += '\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<td> ' + data[index].name + ' </td>\n\t\t\t\t\t\t<td> ' + data[index].value + ' </td>\n\t\t\t\t\t\t<td> ' + data[index].confidence.toFixed(2) * 100 + '% </td>\n\t\t\t\t\t</tr>\n\t\t\t\t';
			}

			wrapper.find('table.table').html(html);
			cardLoad(0);
		};
		var drawSquares = function drawSquares(d) {
			var image = preview.find('img')[0];
			var ratio = image.naturalWidth / image.width;

			preview.find('.dot').remove();

			var keypoints = d.payload.detection.faces["0"].keypoints;

			for (var index in keypoints) {
				var _iteratorNormalCompletion2 = true;
				var _didIteratorError2 = false;
				var _iteratorError2 = undefined;

				try {
					for (var _iterator2 = keypoints[index][Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
						var item = _step2.value;

						preview.append('<div style="transform: translateX(' + item.x / ratio + 'px) translateY(' + item.y / ratio + 'px);" class="dot" />');
					}
				} catch (err) {
					_didIteratorError2 = true;
					_iteratorError2 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion2 && _iterator2.return) {
							_iterator2.return();
						}
					} finally {
						if (_didIteratorError2) {
							throw _iteratorError2;
						}
					}
				}
			}
		};

		if (imageUrl.val().length) {
			face = new Face(imageUrl.val());
			return face.detect().done(console.log).done(printTable).done(showSendBtn).done(drawSquares);
		}

		if (!file[0].files.length) return file.addClass('error');

		face = new Face(file[0].files[0]);
		face.detect().done(console.log).done(printTable).done(showSendBtn).done(drawSquares);
	});

	wrapper.find('input').change(function () {
		wrapper.find('.analyse-ch1-btn').removeAttr('hidden');
	});

	wrapper.find('.send-ch1-btn').click(function (e) {
		e.preventDefault();
		cardLoad(1);
		face.toProfile().always(function () {
			return cardLoad(0);
		});
	});

	fileInputEvents(wrapper);
});

// Face Recognition
$(function () {
	var wrapper = $('.face-recognition-ch1-wrapper');
	var preview = wrapper.find('.image-ch1-wrapper');
	var card = wrapper.find('.card');
	var cardLoad = function cardLoad(a) {
		return a ? card.addClass('loading') : card.removeClass('loading');
	};

	var face = void 0;

	wrapper.find('form').submit(function (e) {
		e.preventDefault();
		cardLoad(1);

		var $t = $(this);
		var file = $t.find('input[name="picture"]');
		var imageUrl = $t.find('input[name="imageUrl"]');

		var getFaces = function getFaces(profileList) {
			return Face.get(profileList.map(function (item) {
				return item.trim();
			}).join(','));
		};
		var insertComparison = function insertComparison(d) {
			var comparison = d.payload.recognition.comparison;
			var arr = [];

			for (var index in comparison) {
				var _iteratorNormalCompletion3 = true;
				var _didIteratorError3 = false;
				var _iteratorError3 = undefined;

				try {
					for (var _iterator3 = comparison[index][Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = _step3.value;

						arr.push(item);
					}
				} catch (err) {
					_didIteratorError3 = true;
					_iteratorError3 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError3) {
							throw _iteratorError3;
						}
					}
				}
			}getFaces(arr.map(function (item) {
				return item.id;
			})).done(function (d) {
				var html = '';
				var _iteratorNormalCompletion4 = true;
				var _didIteratorError4 = false;
				var _iteratorError4 = undefined;

				try {
					for (var _iterator4 = d[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
						var item = _step4.value;

						html += '<li><img src="' + item.imagePath + '" alt="' + item.profileName + '" /> <span>' + item.profileName + '</span> <span>' + (arr.filter(function (i) {
							return i.id == item.profileId;
						}).pop().matchRate * 100).toFixed(2) + '%</span> </li>';
					}
				} catch (err) {
					_didIteratorError4 = true;
					_iteratorError4 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion4 && _iterator4.return) {
							_iterator4.return();
						}
					} finally {
						if (_didIteratorError4) {
							throw _iteratorError4;
						}
					}
				}

				wrapper.find('.reference-ch1-list').html(html);
				cardLoad(0);
			});
		};
		var drawSquares = function drawSquares(d) {
			var image = preview.find('img')[0];
			var ratio = image.naturalWidth / image.width;

			preview.find('.dot').remove();

			var keypoints = d.payload.detection.faces["0"].keypoints;

			for (var index in keypoints) {
				var _iteratorNormalCompletion5 = true;
				var _didIteratorError5 = false;
				var _iteratorError5 = undefined;

				try {
					for (var _iterator5 = keypoints[index][Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
						var item = _step5.value;

						preview.append('<div style="transform: translateX(' + item.x / ratio + 'px) translateY(' + item.y / ratio + 'px);" class="dot" />');
					}
				} catch (err) {
					_didIteratorError5 = true;
					_iteratorError5 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion5 && _iterator5.return) {
							_iterator5.return();
						}
					} finally {
						if (_didIteratorError5) {
							throw _iteratorError5;
						}
					}
				}
			}
		};

		if (imageUrl.val().length) {
			face = new Face(imageUrl.val());
			return face.recognize().done(console.log).done(insertComparison).done(drawSquares);
		}

		if (!file[0].files.length) return file.addClass('error');

		face = new Face(file[0].files[0]);
		face.recognize().done(console.log).done(insertComparison).done(drawSquares);
	});

	wrapper.find('input').change(function () {
		wrapper.find('.recognize-ch1-btn').removeAttr('hidden');
	});

	fileInputEvents(wrapper);
});
"use strict";
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Profile = function () {
	function Profile(name, label) {
		_classCallCheck(this, Profile);

		this.name = name.trim();
		this.label = label.map(function (item) {
			return item.trim();
		});
	}

	_createClass(Profile, [{
		key: 'insert',
		value: function insert() {
			if (!Collection.getCollectionId()) return;
			return $.ajax({
				url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/profile',
				type: 'POST',
				data: {
					'collectionId': Collection.getCollectionId(),
					'profileName': this.name,
					'profileLabel': this.label.join(',')
				}
			});
		}
	}], [{
		key: 'getAll',
		value: function getAll() {
			if (!Collection.getCollectionId()) return;
			return $.ajax({
				url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/profile/',
				type: 'GET',
				data: {
					'_fields[]': 'VC_CollectionID',
					'_values[]': Collection.getCollectionId()
				}
			});
		}
	}, {
		key: 'get',
		value: function get(id) {
			return $.ajax({
				url: 'https://legalbit.com.br/public/christian/index.php/VisageAPI/profile/' + id,
				type: 'GET'
			});
		}
	}, {
		key: 'getProfileId',
		value: function getProfileId() {
			return $('.profile-ch1-wrapper').find('.profile-ch1-list').val();
		}
	}]);

	return Profile;
}();

$(function () {
	var wrapper = $('.profile-ch1-wrapper');
	var list = wrapper.find('.profile-ch1-list');
	var card = wrapper.find('.card');

	var cardLoad = function cardLoad(a) {
		return a ? card.addClass('loading') : card.removeClass('loading');
	};
	var loadProfiles = function loadProfiles() {
		cardLoad(1);
		return Profile.getAll().done(function (d) {
			var html = '';
			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = d[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var item = _step.value;

					html += '<option value="' + item.VC_ProfileID + '">' + item.name + '</option>';
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}

			list.html(html).find('option').not('[disabled]').first().attr('selected', 'selected');
			cardLoad(0);
		});
	};

	wrapper.on('reload', loadProfiles);

	wrapper.find('form').submit(function (e) {
		var _this = this;

		cardLoad(1);
		e.preventDefault();

		var name = $(this).find('input[name="profileName"]').removeClass('error');
		var label = $(this).find('input[name="profileLabel"]').removeClass('error');

		if (!(name.val() || "").length) return name.addClass('error');
		if (!(label.val() || "").length) return label.addClass('error');

		var pro = new Profile(name.val(), label.val().split(','));
		pro.insert().done(function (d) {
			return d.status.toLowerCase() != "ok" ? input.addClass('error') : null;
		}).always(function () {
			return cardLoad(0);
		}).done(function () {
			loadProfiles().done(function () {
				return list.find('option').filter(function (i, el) {
					return $(el).text() == pro.name;
				}).first().attr('selected', 'true').parent().change();
			});
		}).done(function () {
			return $(_this).find('input').val('').change();
		});
	});

	loadProfiles();
});
//# sourceMappingURL=app.js.map
